﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{

    public float speed;
    void Update()
    {
        if (Input.GetKey("w")){
            transform.Rotate(speed * Time.deltaTime, 0, 0);
        }else if (Input.GetKey("s")){
            transform.Rotate(-speed * Time.deltaTime, 0, 0);
        }else if (Input.GetKey("a")){
            transform.Rotate(0, speed * Time.deltaTime, 0);
        }else if (Input.GetKey("d")){
            transform.Rotate(0, -speed * Time.deltaTime, 0);
        }
    }
}
