﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlanetBehaviour : MonoBehaviour
{

    public GameObject projectile;

    private GameObject centralBody;
    private GameObject system;
    private SunBehaviour cbScript;
    private SystemBehaviour systemScript;

    //Planet Variables
    public float mass, periodDays, apogee, perogee;

    private Rigidbody rb;
    private ConstantForce cf;


    const float G = 6.67408e-11f;
    float r, F;


   Vector3 F_dir;

    void InstantiatePlanet(){
        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
        cf = GetComponent<ConstantForce>();
        system = GameObject.Find("/System");
        centralBody = GameObject.Find("/System/Sun");
        cbScript = centralBody.GetComponent<SunBehaviour>();
        systemScript = system.GetComponent<SystemBehaviour>();
    }


    void UpdateValues(Vector3 Distance){
        F_dir = Vector3.Normalize(Distance);
        r = Distance.magnitude;
        F = (G * cbScript.mass * rb.mass) / (float)Math.Pow(r, 2);
        F *= 10e-9f * (float)Math.Pow(systemScript.timescale, 2);
        cf.force = F_dir * F;
    }

    Vector3 StartingVelocity(){
        float speed = ((2 * (float)Math.PI)/(periodDays * 24f * 60f * 60f)) * r * systemScript.timescale;
        Vector3 direction = new Vector3(0, 0, 1);
        return speed*direction;
    }



    void Start()
    {
        InstantiatePlanet();
        UpdateValues(cbScript.transform.position - this.transform.position);
        rb.velocity = StartingVelocity();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateValues(cbScript.transform.position - transform.position);

        if (Input.GetKeyDown("space"))
        {
            GameObject p = Instantiate(projectile, this.transform.position, Quaternion.identity);
            ProjectileBehaviour pScript = p.GetComponent<ProjectileBehaviour>();
            pScript.InstantiateProjectile(1f, rb.velocity * 1.09f);
        }
    }

}
