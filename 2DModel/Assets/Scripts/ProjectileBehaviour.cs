﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProjectileBehaviour : MonoBehaviour
{



    public GameObject centralBody;
    public GameObject system;
    public SunBehaviour cbScript;
    private SystemBehaviour systemScript;

    private Rigidbody rb;
    private ConstantForce cf;


    const float G = 6.67408e-11f;
    float r, F;


   Vector3 F_dir;

    public void InstantiateProjectile(float mass, Vector3 velocity){
        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
        rb.velocity = velocity;
        cf = GetComponent<ConstantForce>();
        system = GameObject.Find("/System");
        centralBody = GameObject.Find("/System/Sun");
        cbScript = centralBody.GetComponent<SunBehaviour>();
        systemScript = system.GetComponent<SystemBehaviour>();
    }


    void UpdateValues(Vector3 Distance){
        F_dir = Vector3.Normalize(Distance);
        r = Distance.magnitude;
        F = (G * cbScript.mass * rb.mass) / (float)Math.Pow(r, 2);
        F *= 10e-12f * (float)Math.Pow(systemScript.timescale, 2);
        cf.force = F_dir * F;
    }


    // Update is called once per frame
    void Update()
    {
        Debug.Log(cbScript.transform.position);
        //UpdateValues(cbScript.transform.position - this.transform.position);
    }

}
