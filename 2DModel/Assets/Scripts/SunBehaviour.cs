﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SunBehaviour : MonoBehaviour
{
    private Rigidbody rb;
    public float mass;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
    }
}
