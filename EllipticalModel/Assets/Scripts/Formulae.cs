﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Formulae : MonoBehaviour
{
    public static double G = 6.67408e-11, massRatio = 1e22, distRatio = 1e9, timeRatio = 86400;
    

    // ForceDirection is the normalized vector from the orbiting body, to the central body.
    // ForceMagnitude = GMm/(r^2).
    public static Vector3 CalculateF(double mass1, double mass2, Vector3 pos1, Vector3 pos2, float timescale)
    {
        Vector3 fDir = Vector3.Normalize(pos2 - pos1);
        double  r = (pos2 - pos1).magnitude * distRatio;
        
		double  F = (G * mass1 * mass2 * 10e21) / Math.Pow(r, 2);

        F *= Math.Pow(timeRatio * timescale, 2) / distRatio;

        return fDir * (float) F;
    }

    
    //instantaneous velocity = root(GM * ( (2/r) / (1/a) ))
    //used fo calculating the injection velocity to go from home orbit marker, to target orbit marker
	public static double InstantaneousVelocity(float cbMass, double semiMajorAxis, float radius, float timescale)
	{
		double m = cbMass * massRatio;
		double r = radius * distRatio;
		double a = semiMajorAxis * distRatio;
		
		double speed = Math.Sqrt (G * m * ((2 / r) - (1 / a)));

		return (speed * timeRatio * timescale) / distRatio;
	}
	        
	public static Vector3 CalculatePosition(float angle, double trueAnomaly, double semiMajorAxis, double eccentricity){
		
		double TA = (double)trueAnomaly * (Math.PI / 180);
		Vector3 direction = Quaternion.AngleAxis ((float)angle, Vector3.back) * Vector3.right;
		float distance = (float)CalculateDistance (semiMajorAxis, eccentricity, TA);
		return direction * distance;
	}


    private static double CalculateMAfromTSP(double meanMotion, float timeSincePeri)
    {
        return meanMotion * timeSincePeri;
    }

	private static double CalculateEAfromMA(double MA, double e)
	{
		double err = 1, EA = Math.Atan2 (Math.Sin (MA), Math.Cos (MA) - e);
		while (Math.Abs (err) > 0.000001) {
			err = ( EA - e*Math.Sin(EA) - MA ) / (1 - e*Math.Cos(EA));
			EA -= err;
		}
		return EA;
	}

    public static double CalculateTAfromEA(double eccentricity, double eccentricAnomaly)
    {
        return  2 * Math.Atan2(
            Math.Pow(1 + eccentricity, 0.5) * Math.Sin(eccentricAnomaly / 2),
            Math.Pow(1 - eccentricity, 0.5) * Math.Cos(eccentricAnomaly / 2));
    }

    private static double CalculateDistance(double semiMajorAxis, double eccentricity, double trueAnomaly)
    {
        return semiMajorAxis*((1-Math.Pow(eccentricity, 2))/(1 + Math.Cos(trueAnomaly)*eccentricity));
    }

    public static (double TA, Vector3 pos) CalculatePositionVector(
        float timeSincePeri, double eccentricity, double semiMajorAxis, double period, double longitudeOfPeriapsis, string name)
    {
		double meanMotion, meanAnomaly, eccentricAnomaly, trueAnomaly, magnitude, TA;
		float angle;
		Vector3 direction, pos;

        meanMotion = CalculateMMfromP(period);
        meanAnomaly = CalculateMAfromTSP(meanMotion, timeSincePeri);
        eccentricAnomaly = CalculateEAfromMA(meanAnomaly, eccentricity);
        trueAnomaly = CalculateTAfromEA(eccentricity, eccentricAnomaly);
        magnitude = CalculateDistance(semiMajorAxis, eccentricity, trueAnomaly);

		TA = (trueAnomaly * (180 / Math.PI));
		angle = (float)longitudeOfPeriapsis + (float)TA;
		if (angle > 360f)	angle -= 360f;
		direction = Vector3.Normalize(Quaternion.AngleAxis (angle, Vector3.back) * Vector3.right);
		pos = (float)magnitude * direction;

        return (TA, pos);
    }

	//used in finding the time since periapsis from true anomaly
	private static double CalculateEAfromTA(double eccentricity, double trueAnomaly){
		return 2 * Math.Atan2(
			Math.Pow(1 - eccentricity, 0.5) * Math.Sin(trueAnomaly / 2),
			Math.Pow(1 + eccentricity, 0.5) * Math.Cos(trueAnomaly / 2)
		);
	}
	//used in finding the time since periapsis from true anomaly
	private static double CalculateMAfromEA(double eccentricAnomaly, double eccentricity){
		return eccentricAnomaly - (eccentricity * Math.Sin(eccentricAnomaly));
	}

	//used in finding the time since periapsis from true anomaly
	private static double CalculateMMfromP(double period)
	{
		return (2*Math.PI) / period;
	}

	//used in finding the time since periapsis from true anomaly
	private static double CalculateTSPfromMM(double meanMotion, double meanAnomaly){
		return meanAnomaly / meanMotion;
	}

	public static double CalculateTSPfromTA(double trueAnomaly, double eccentricity, double period){
		double eccentricAnomaly, meanAnomaly, meanMotion, timeSincePeriapsis;
		trueAnomaly = trueAnomaly * Math.PI / 180;
		eccentricAnomaly = CalculateEAfromTA (eccentricity, trueAnomaly);
		meanAnomaly = CalculateMAfromEA (eccentricAnomaly, eccentricity);
		meanMotion = CalculateMMfromP (period);
		timeSincePeriapsis = CalculateTSPfromMM (meanMotion, meanAnomaly);
		
		return (float)timeSincePeriapsis;
	}

}
