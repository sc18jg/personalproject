﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OrbitMarker
{
	private float massStar, G = 6.67408e-11f, timescale;

	public DateTime[] dateTime { get; set; }

	public Vector3 position { get; }
	public double trueAnomaly { get; }
	public Vector3 injectV;


	public OrbitMarker(float M, DateTime date, Vector3 pos, float timeSc, double trueAnom, double period)
	{
		massStar = M;
		position = pos;
		timescale = timeSc;
		trueAnomaly = trueAnom;
		dateTime = new DateTime[3];

		for (int i=0; i<3; i++) {
			dateTime[i] = date.AddDays(period*i);
		}
	}


	public void CalcInjectVelocity(OrbitMarker target){ 
		double v = Formulae.InstantaneousVelocity (massStar, (this.position.magnitude + target.position.magnitude) / 2, this.position.magnitude, timescale);
		Vector3 dir = Vector3.Normalize (Vector3.Cross (-position, Vector3.back));
		injectV = (float)v * dir;
	}

	//calculate arrival date to compare with the target planet.
	public DateTime CalcArrivalDate(OrbitMarker target){
		double a = ((this.position.magnitude + target.position.magnitude) / 2) * Formulae.distRatio;
		double M = massStar * Formulae.massRatio;

		double flightDurationSeconds = Math.Sqrt ( (4 * Math.Pow(Math.PI, 2) * Math.Pow(a, 3)) / ( G * M) );

		return dateTime[0].AddSeconds (flightDurationSeconds/2);
	}

	//compares arrival date with the target position date. if within sensitivity, then it is a good launch opportunity.
	public bool IsGoodLaunch(OrbitMarker target, float sens){
		DateTime arrivalDate = CalcArrivalDate (target);
		if (
			(arrivalDate > target.dateTime[0].AddDays (-sens) && arrivalDate < target.dateTime[0].AddDays (sens)) || 
			(arrivalDate > target.dateTime[1].AddDays (-sens) && arrivalDate < target.dateTime[1].AddDays (sens)) || 
			(arrivalDate > target.dateTime[2].AddDays (-sens) && arrivalDate < target.dateTime[2].AddDays (sens)) ){
			return true;
		} else {
			return false;
		}
	}

}
