﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class PlanetBehaviour2 : MonoBehaviour
{

    public GameObject projectile;
	public GameObject orbitMarkerGhost;
	public GameObject targetMarker, launchMarker;

    private GameObject centralBody;
    private GameObject system;
    private SunBehaviour cbScript;
    private SystemBehaviour systemScript;

	private GameObject targetPlanet;
	private PlanetBehaviour2 targetPlanetScript;

    //Planet Variables
	public double semiMajorAxisAU, eccentricity, inclination, longitudeOfAscendingNode, longitudeOfPerihelion, period, LASensitivity;
	public string periapsisDate;
    public bool shootable;
	public string target;

	private double trueAnomaly, argumentOfPeriapsis, semiMajorAxis;
	private float timeSincePeri;
	private bool launched, targeted, orbitChecked;

	private OrbitMarker[] orbit;    
	private OrbitMarker launch;

	private DateTime prevPeriapsisDateTime;
	private int N = 1000;



    void InstantiatePlanet(){
	
		//To allow access to other game object's data
        system = GameObject.Find("/System");
        centralBody = GameObject.Find("/System/Sun");
        cbScript = centralBody.GetComponent<SunBehaviour>();
        systemScript = system.GetComponent<SystemBehaviour>();

		//if a planet is shootable, then collect target data
		if (shootable) {
			targetPlanet = GameObject.Find("/System/" + target);
			targetPlanetScript = targetPlanet.GetComponent<PlanetBehaviour2>();
		}
		//creating local data for planet
		semiMajorAxis = semiMajorAxisAU * 149.597870700f;
		argumentOfPeriapsis = longitudeOfPerihelion - longitudeOfAscendingNode;
		if (argumentOfPeriapsis < 0) { argumentOfPeriapsis += 360; }
		prevPeriapsisDateTime = DateTime.Parse (periapsisDate);
		if ((systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays > period) {
			int years = (int)((systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays / period);
			prevPeriapsisDateTime = prevPeriapsisDateTime.AddDays (period * years);
		} else if ((systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays < 0) {
			int years = -1 + (int)((systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays / period);
			prevPeriapsisDateTime = prevPeriapsisDateTime.AddDays (period * years);
		}
		

		timeSincePeri = (float)(systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays;
		
		orbit = new OrbitMarker[N];
		double TA;
		float angle;
		DateTime date;
		Vector3 pos;

		//creating orbit markers
		for (int i = 0; i<N; i++) {
			angle = (360f / N) * i;
			TA = (double)angle - argumentOfPeriapsis - longitudeOfAscendingNode;
			if (TA < 0) {
				TA += 360;
			}

			date = prevPeriapsisDateTime.AddDays (Formulae.CalculateTSPfromTA (TA, eccentricity, period));
			pos = Formulae.CalculatePosition (angle, TA, semiMajorAxis, eccentricity);
			orbit [i] = new OrbitMarker (cbScript.mass, date, pos, systemScript.timescale, TA, period);
			GameObject om = Instantiate( orbitMarkerGhost, pos, Quaternion.identity);
			om.transform.parent = centralBody.transform;
		}

    }
	//checks each orbit marker for a launch opportunity
	void CheckForLaunches() {
		int i, opposite;
		orbitChecked = true;
		for (i = 0; i<orbit.Length; i++) {
			opposite = i + N / 2;
			if (opposite >= N) {
				opposite -= N;
			}
			if (!targeted && DateTime.Compare (orbit[i].dateTime[0], systemScript.currentDateTime) > 0) {
				if (orbit [i].IsGoodLaunch (targetPlanetScript.orbit [opposite], .5f)){
					targeted = true;
					GameObject lm = Instantiate( launchMarker, orbit[i].position, Quaternion.identity);
					GameObject tm = Instantiate( targetMarker, targetPlanetScript.orbit [opposite].position, Quaternion.identity);
					launch = orbit[i];
					launch.CalcInjectVelocity(targetPlanetScript.orbit[opposite]);

					double tempTA = targetPlanetScript.orbit [opposite].trueAnomaly;
					Debug.Log("Launch from " + this.name + " at: " + orbit[i].dateTime[0].ToString());
					Debug.Log("ETA to " + target + " at: " + orbit[opposite].dateTime[0].ToString());
				}
			}			
		}
	}
	//called every periapsis, updates orbitmarker dates and rechecks for launch opportunities
	void UpdateOrbit(){
		orbitChecked = false;
		prevPeriapsisDateTime = prevPeriapsisDateTime.AddDays (period);
		int i, j;
		for (i = 0; i<N; i++) {
			for (j = 0; j<3; j++){
				orbit[i].dateTime[j] = orbit[i].dateTime[j].AddDays(period);
			}
		}
		if (target.Length >= 1 && shootable) {
			CheckForLaunches();
		}

	}

	//called every frame, updates planet position.
    void UpdateValues()
    {
		if (timeSincePeri > period) {
			UpdateOrbit();
		}
        (trueAnomaly, transform.position) = Formulae.CalculatePositionVector(timeSincePeri, eccentricity, semiMajorAxis, period, argumentOfPeriapsis + longitudeOfAscendingNode, this.name);
        timeSincePeri = (float)(systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays;

    }


	void LaunchProjectile(){
		Debug.Log ("Launching projectile from " + this.name + " to " + target + ".");
		launched = true;
		
		GameObject p = Instantiate (projectile, transform.position, Quaternion.identity);
		p.transform.parent = system.transform;
		ProjectileBehaviour pScript = p.GetComponent<ProjectileBehaviour> ();
				
		pScript.InstantiateProjectile (1f, launch.injectV, this.name, target);
		
	}




    void Start(){ InstantiatePlanet(); }

    // Update is called once per frame
    void Update()
    {
		if (orbitChecked == false && target.Length >= 1 && shootable) {
			CheckForLaunches();
		}
		timeSincePeri = (float)(systemScript.currentDateTime - prevPeriapsisDateTime).TotalDays;
        UpdateValues();

		//check if launch conditions are met, if so launch projectile.
		if (targeted == true && launched == false) {
			if (trueAnomaly > launch.trueAnomaly - LASensitivity && trueAnomaly < launch.trueAnomaly + LASensitivity) {
				LaunchProjectile ();
			}
		}
    }

}
