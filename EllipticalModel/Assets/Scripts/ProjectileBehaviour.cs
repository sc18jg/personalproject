﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProjectileBehaviour : MonoBehaviour
{



    private GameObject system;
    private SystemBehaviour systemScript;
    private GameObject[] planets;
	private PlanetBehaviour2[] planetScripts;
	private GameObject[] stars;
	private SunBehaviour[] starScripts;

	private String launch, target;

	private DateTime launchTime, collisionTime;

    private Rigidbody rb;
    private ConstantForce cf;

	private Vector3 launchPlanetPos, targetPlanetPos;
	private float transferTime = 0.0f;


	private float debugApoapsis, debugPeriapsis;
    
    public void InstantiateProjectile(float mass, Vector3 velocity, String lau, String tar){
		
        system = GameObject.Find("/System");
        systemScript = system.GetComponent<SystemBehaviour>();

		stars = GameObject.FindGameObjectsWithTag("Star");
		starScripts = FindObjectsOfType<SunBehaviour>();

        planets = GameObject.FindGameObjectsWithTag("Planet");
		planetScripts = FindObjectsOfType<PlanetBehaviour2>();

        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
		rb.velocity = velocity;
        cf = GetComponent<ConstantForce>();

		launch = lau;
		target = tar;

		for (int i = 0; i<planetScripts.Length; i++) {
			if (planetScripts[i].name == launch){ launchPlanetPos = planetScripts[i].transform.position;}
			else if (planetScripts[i].name == target){targetPlanetPos = planetScripts[i].transform.position;}
		}
		launchTime = systemScript.currentDateTime;


		debugApoapsis = transform.position.magnitude;
		debugPeriapsis = transform.position.magnitude;
    }

	//Projectile data output to console when projectile collides with its target.
    private void OnTriggerEnter(Collider col)
    {
        if (col.name == target) {
			collisionTime = systemScript.currentDateTime;
			transferTime = (float)(collisionTime - launchTime).TotalDays;
			Debug.Log ("Collision!");
			Debug.Log ("transferTime: " + transferTime);
			float angle = Vector3.Angle(launchPlanetPos, targetPlanetPos);
			Debug.Log ("Angle: " + angle);
			Destroy(gameObject);
		}
    }

    void UpdateValues()
    {
		int i;
		//Calculate gravitational influence acting on this body
		cf.force = new Vector3 (0, 0, 0);
		
		for (i = 0; i < stars.Length; i++) 
		{
			cf.force += Formulae.CalculateF (rb.mass, starScripts[i].mass, transform.position,
			                                 stars[i].transform.position, systemScript.timescale);
		}
		transferTime += Time.deltaTime;
    }
    void Update()
    {
        UpdateValues();
    }

}
                                                                                                                                   