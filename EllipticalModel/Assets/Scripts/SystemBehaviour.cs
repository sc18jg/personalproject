﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SystemBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    public float timescale;
	public string startDateTime;
	public string currentTime;

	public DateTime currentDateTime;

	void Start(){ 
		currentDateTime = DateTime.Parse (startDateTime); 
	}

	void Update(){ 
		currentDateTime = currentDateTime.AddDays (Time.deltaTime * timescale); 
		currentTime = currentDateTime.ToString ();
	}
}
