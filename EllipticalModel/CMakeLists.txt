cmake_minimum_required(VERSION 3.13)
project(2DModel C)

set(CMAKE_C_STANDARD 11)

include_directories(.)
include_directories(Library)
include_directories(Library/PackageCache)
include_directories(Library/PackageCache/com.unity.ads@2.0.8)
include_directories(Library/PackageCache/com.unity.ads@2.0.8/Editor)
include_directories(Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources)
include_directories(Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS)
include_directories(Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds)
include_directories(Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework)
include_directories(Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers)

add_executable(2DModel
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UADSInAppPurchaseMetaData.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UADSJsonStorage.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UADSMediationMetaData.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UADSMetaData.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UADSPlayerMetaData.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UnityAds.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UnityAdsExtended.h
        Library/PackageCache/com.unity.ads@2.0.8/Editor/Resources/iOS/builds/UnityAds.framework/Headers/UnityAdsUnityDelegate.h)
