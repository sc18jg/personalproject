To run the code it is necessary to have a working install of the Unity Engine.
In the Unity Hub, under installs, add an install of Unity 2018.

To add the project to the Unity hub make sure atleast one project exists in the projects list, or else the Add project feature will not work. (BUG)
This can be fixed by creating a new project, saving it, and then closing it. On re-opening of the Unity Hub, you should be able to add a project.

Add 'EllipticalModel' to the project list and select the Unity 2018 version for it to run on, and then open it. It should run first time from clicking the play button.

Notes for running application:

- Do not change the timescale value during runtime as this can lead to discrepencies between the Newtonian and Position-Time model.
This can only be done if and only if the timescale is the same when the launch orbit marker is calculated and when simulating the projectile's launch and transfer.